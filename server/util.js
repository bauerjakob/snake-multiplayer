function getRandomColor() {
    const colors = [
        '#3F51B5',
        '#FFB300',
        '#D84315',
        '#388E3C',
        '#C2185B',
        '#FFEB3B',
        '#3F51B5',
        '#673AB7',
        '#9C27B0'
    ];

    return colors[Math.floor(Math.random() * colors.length)];
}

module.exports = {
    getRandomColor
}