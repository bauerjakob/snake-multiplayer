const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http, { cors: { origin: '*' } });
const cors = require('cors');
const uid = require('uid');
const { createGameState, getPlayerById, addPlayerToGame, fireBullet } = require('./game');
const port = process.env.PORT || 3000;

app.use(cors());

let gameState = {};
let clientRooms = {};

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

http.listen(port, () => {
    console.log('listening on port', port);
});

io.on('connection', (socket) => {
    socket.on('keydown', onKeydown);
    socket.on('newRoom', onNewRoom);
    socket.on('joinRoom', onJoinRoom);
    socket.on('disconnect', onDisconnect);

    function onDisconnect() {
        const roomName = clientRooms[socket.id];
        if (!roomName) {
            return;
        }

        gameState[roomName].players = gameState[roomName].players.filter(p => p.id != socket.playerId);

        if (gameState[roomName].players.length == 0) {
            gameState[roomName] = undefined;
        }
    }

    function onNewRoom() {
        let roomName = uid.uid(4);

        addClientToRoom(roomName, socket);

        gameState[roomName] = createGameState();
        gameState[roomName].roomName = roomName;
        addPlayerToGame(gameState[roomName], socket.playerId, io);
    }

    function onJoinRoom(roomName) {
        const room = io.sockets.adapter.rooms.get(roomName);

        if (!room || room.size == 0 || room.has(socket.id)) {
            socket.emit('unknownCode');
            return;
        }
        socket.emit('successfullyJoined');

        addClientToRoom(roomName, socket);
        addPlayerToGame(gameState[roomName], socket.playerId, io);
    }
    function onNewRoom() {
        let roomName = uid.uid(4);

        addClientToRoom(roomName, socket);

        gameState[roomName] = createGameState();
        gameState[roomName].roomName = roomName;
        addPlayerToGame(gameState[roomName], socket.playerId, io);
    }

    function onJoinRoom(roomName) {
        const room = io.sockets.adapter.rooms.get(roomName);

        if (!room || room.size == 0 || room.has(socket.id)) {
            socket.emit('unknownCode');
            return;
        }
        socket.emit('successfullyJoined');

        addClientToRoom(roomName, socket);
        addPlayerToGame(gameState[roomName], socket.playerId, io);
    }

    function onKeydown(keyCode) {
        if (!keyCode) {
            return;
        }
        keyCode = parseInt(keyCode);

        const roomName = clientRooms[socket.id];
        if (!roomName) {
            return;
        }
        let players = gameState[roomName].players;
        let player = getPlayerById(players, socket.playerId);


        // space pressed 
        if (keyCode == 32) {
            fireBullet(gameState[roomName], player.id, io);
            return;
        }

        
        let data = {};
        // arrow pressed
        switch (keyCode) {
            case 37: // arrow left
                data.x = -1;
                data.y = 0; 
                break;
            case 38: // arrow up
                data.x = 0;
                data.y = -1; 
                break;
            case 39: // arrow right
                data.x = 1;
                data.y = 0; 
                break;
            case 40: // arrow down
                data.x = 0;
                data.y = 1; 
                break;
            default:
                return;
        }

        if (player.snake.length > 1 && 
            (Math.abs(player.vel.x) == Math.abs(data.x) || Math.abs(player.vel.y) == Math.abs(data.y))) {
            return;
        }
        player.newVel = data; 
    }
});


function addClientToRoom(roomName, socket) {
    clientRooms[socket.id] = roomName;
    socket.join(roomName);
    socket.playerId = uid.uid(8);
    socket.emit('gameInfos', { roomName: roomName, playerId: socket.playerId });
}

module.exports = {
    io: io
}

