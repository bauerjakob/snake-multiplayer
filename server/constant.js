const FRAME_RATE = 10;
const GRID_SIZE = 40;
const PLAYER_MAX_SPEED = 70;
const PLAYER_START_SPEED = 250;
const BULLET_SPEED = 45;
const FOOD_COUNT = 6;

module.exports = {
  FRAME_RATE,
  GRID_SIZE,
  PLAYER_START_SPEED,
  PLAYER_MAX_SPEED,
  BULLET_SPEED,
  FOOD_COUNT
}