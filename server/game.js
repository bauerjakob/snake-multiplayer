const { uid } = require("uid");
const { GRID_SIZE, PLAYER_START_SPEED, PLAYER_MAX_SPEED, BULLET_SPEED, FOOD_COUNT } = require("./constant");
const { getRandomColor } = require("./util");

function createGameState() {
    var state = {
        players: [
        ],
        food: [
        ],
        bullets: [
        ],
        gridSize: GRID_SIZE,
    };

    for (let i = 0; i < FOOD_COUNT; i++) {
        drawRandomFood(state);
    }

    return state;
}

// player
function addPlayerToGame(state, playerId, io) {
    let newPlayer = {
        id: playerId,
        tailNotAdded: 0,
        bulletCount: 0,
        color: getRandomColor(),
        newVel: {
            x: 0,
            y: 1,
        },
        vel: {
            x: 0,
            y: 1,
        },
        pos: {
            x: 3,
            y: 10
        },
        speed: PLAYER_START_SPEED,
        snake: [
            { x: 1, y: 10 },
            { x: 2, y: 10 },
            { x: 3, y: 10 },
        ],
    }
    state.players.push({ ...newPlayer })
    gameLoopPlayer(state, newPlayer.id, io);
}

function gameLoopPlayer(state, playerId, io) {
    let player = getPlayerById(state.players, playerId);

    if (!player) {
        return;
    }
    movePlayer(player);
    checkIfPlayerHitFood(state, player, io);
    checkIfPlayerHitOtherPlayer(state, player, io);
    updateGameState(io, state);
    setTimeout(() => gameLoopPlayer(state, playerId, io), player.speed);
}

function movePlayer(player) {
    addVelToPlayer(player);

    player.snake.push({ ...player.pos })

    if (player.tailNotAdded == 0) {
        player.snake.shift();
    } else {
        player.tailNotAdded--;
    }
}

function addVelToPlayer(player) {
    player.vel = player.newVel;
    player.pos.x = (player.pos.x += player.vel.x) % GRID_SIZE
    player.pos.y = (player.pos.y += player.vel.y) % GRID_SIZE

    if (player.pos.x < 0) {
        player.pos.x = GRID_SIZE
    }

    if (player.pos.y < 0) {
        player.pos.y = GRID_SIZE
    }
}

function checkIfPlayerHitFood(state, player, io) {
    for (let food of state.food) {
        if (player.pos.x == food.x && player.pos.y == food.y) {
            state.food = state.food.filter(f => f.id != food.id);
            player.speed *= 0.87;
            if (player.speed < PLAYER_MAX_SPEED) {
                player.speed = PLAYER_MAX_SPEED;
            }
            player.tailNotAdded += Math.floor(Math.random() * 5) + 1;
            addBulletToPlayer(player, state.roomName, io);
            drawRandomFood(state);
        }
    }
}

function checkIfPlayerHitOtherPlayer(state, player, io) {
    for (let p of state.players) {
        for (let i = 0; i < p.snake.length; i++) {
            let tailPart = p.snake[i];
            if (player.id == p.id && i == p.snake.length - 1) {
                continue;
            }

            if (player.pos.x == tailPart.x && player.pos.y == tailPart.y) {
                player.snake = player.snake.slice(0, 1);
                afterPlayerHitSomething(state, player, io);
                if (i == p.snake.length - 1) {
                    p.snake = p.snake.slice(0, 1);
                    afterPlayerHitSomething(state, p, io);
                }
            }
        }
    }
}

function afterPlayerHitSomething(state, player, io) {
    player.tailNotAdded = 0;
    player.bulletCount = 0;
    player.speed = PLAYER_START_SPEED;
    updateBulletState(io, state.roomName, player.id, player.bulletCount);
}

// bullet
function fireBullet(state, playerId, io) {
    let player = getPlayerById(state.players, playerId);
    if (player.bulletCount <= 0) {
        return;
    }
    updateBulletState(io, state.roomName, playerId, --player.bulletCount);
    let newBullet = {
        id: uid(8),
        firedByPlayerId: playerId,
        color: player.color,
        speed: BULLET_SPEED,
        pos: {
            x: player.pos.x,
            y: player.pos.y
        },
        vel: player.vel
    }
    state.bullets.push({ ...newBullet });

    gameLoopBullet(state, newBullet.id, io);
}

function gameLoopBullet(state, bulletId, io) {
    bullet = getBulletById(state.bullets, bulletId);
    moveBullet(bullet);
    let hitSomething = checkIfBulletHitSomething(state, bullet, io);
    updateGameState(io, state);
    if (!hitSomething) {
        setTimeout(() => gameLoopBullet(state, bulletId, io), bullet.speed);
    }
}

function moveBullet(bullet) {
    bullet.pos.x = (bullet.pos.x += bullet.vel.x);
    bullet.pos.y = (bullet.pos.y += bullet.vel.y);
}

function addBulletToPlayer(player, roomName, io) {
    if (Math.floor(Math.random() * 3) == 0) {
        player.bulletCount += 1;
        updateBulletState(io, roomName, player.id, player.bulletCount);
    }

}

function checkIfBulletHitSomething(state, bullet, io) {
    if (bullet.pos.x < 0 || bullet.pos.x > GRID_SIZE
        || bullet.pos.y < 0 || bullet.pos.y > GRID_SIZE) {
        state.bullets = state.bullets.filter(b => b.id != bullet.id);
        return true;
    }

    for (let player of state.players) {
        for (let i = 0; i < player.snake.length; i++) {
            let tailPart = player.snake[i];
            if (bullet.pos.x == tailPart.x && bullet.pos.y == tailPart.y) {
                player.snake = player.snake.slice(Math.max(i, 1));
                afterPlayerHitSomething(state, player, io);
                state.bullets = state.bullets.filter(b => b.id != bullet.id);
                updateGameState(io, state);
                return true;
            }
        }
    }

    for (let food of state.food) {
        if (bullet.pos.x == food.x && bullet.pos.y == food.y) {
            state.food = state.food.filter(f => f.id != food.id);
            let player = getPlayerById(state.players, bullet.firedByPlayerId);
            player.tailNotAdded += Math.floor(Math.random() * 5) + 1;
            addBulletToPlayer(player, state.roomName, io);
            drawRandomFood(state);
            return false;
        }
    }

    return false;
}

// food
function drawRandomFood(state) {
    food = {
        id: uid(8),
        x: Math.floor(Math.random() * GRID_SIZE),
        y: Math.floor(Math.random() * GRID_SIZE),
    }

    state.food.push({ ...food });
}


// update state
function updateGameState(io, state) {
    io.sockets.in(state.roomName)
        .emit('stateChanged', state);
}

function updateBulletState(io, roomName, playerId, bulletCount) {
    io.sockets.in(roomName)
        .emit('bulletCount', { playerId: playerId, bulletCount: bulletCount });
}

// helper
function getPlayerById(players, playerId) {
    return players.filter(p => p.id == playerId)[0];
}


function getBulletById(bullets, bulletId) {
    return bullets.filter(b => b.id == bulletId)[0];
}

module.exports = {
    createGameState,
    getPlayerById,
    addPlayerToGame,
    fireBullet
};