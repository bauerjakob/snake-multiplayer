

const BG_COLOUR = '#231f20';
const SNAKE_COLOUR = '#c2c2c2';
const FOOD_COLOUR = '#e66916';

const gameScreen = document.getElementById('gameScreen');
const startScreen = document.getElementById('startScreen');
const newGameBtn = document.getElementById('newGameButton');
const joinGameBtn = document.getElementById('joinGameButton');
const gameCodeInput = document.getElementById('gameCodeInput');
const gameCodeDisplay = document.getElementById('gameCodeDisplay');

let canvas, ctx;
let playerId;

// const SNAKE_BODY_IMAGE = new Image();
// SNAKE_BODY_IMAGE.src = 'resources/images/background.jpg';   

// const SNAKE_HEAD_IMAGE = new Image();
// SNAKE_HEAD_IMAGE.src = 'https://cdn.pixabay.com/photo/2013/07/13/11/34/apple-158419_960_720.png';

// const FOOD_IMAGE = new Image();
// FOOD_IMAGE.src = 'https://cdn.pixabay.com/photo/2013/07/13/11/34/apple-158419_960_720.png';

newGameBtn.addEventListener('click', onNewGame);
joinGameBtn.addEventListener('click', onJoinGame);

const socket = io("http://server:3000");
socket.on('stateChanged', onStateChanged);
socket.on('bulletCount', onBulletCount);
socket.on('gameInfos', onGameInfos);
socket.on('unknownCode', onUnknownCode);
socket.on('successfullyJoined', onSuccessfullyJoined)


function onNewGame() {
    socket.emit('newRoom');
    startGame();
}

function onJoinGame() {
    const code = gameCodeInput.value.toLowerCase();
    socket.emit('joinRoom', code);
}

function onSuccessfullyJoined() {
    startGame(); let canvas, ctx;
    let playerId;
}

function onUnknownCode() {
    alert("Room does not exist.")
}

function onGameInfos(init) {
    gameCodeDisplay.innerText = init.roomName;
    playerId = init.playerId;
}



function onStateChanged(state) {
    requestAnimationFrame(() => paintGame(state))
}

function onBulletCount(bulletCount) {
    console.log(bulletCount, playerId)
    if (bulletCount.playerId == playerId) {
        document.getElementById('bulletCountDisplay').innerText = bulletCount.bulletCount;
    }
}

function paintGame(state) {
    paintBackground();
    const gridSize = state.gridSize;
    const size = canvas.width / gridSize;

    paintBullets(state.bullets, size);
    paintPlayers(state.players, size);
    paintFood(state.food, size, FOOD_COLOUR);
}

function paintBackground() {
    // var background = new Image();
    // background.src = "https://cdn.pixabay.com/photo/2016/04/18/16/22/watercolour-1336856__340.jpg";
    // ctx.drawImage(background, 0, 0);

    ctx.fillStyle = BG_COLOUR;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
}

function paintFood(food, size, color) {
    ctx.fillStyle = color;
    for (let f of food) {
        // ctx.drawImage(FOOD_IMAGE, f.x * size, f.y * size, size, size);
        ctx.fillRect(f.x * size, f.y * size, size, size);
    }
}

function paintBullets(bullets, size) {
    bSize = size / 2;
    for (let bullet of bullets) {
        ctx.fillStyle = bullet.color;
        ctx.fillRect(bullet.pos.x * size + bSize / 2, bullet.pos.y * size + bSize / 2, bSize, bSize);
    }
}

function paintPlayers(players, size,) {
    for (let player of players) {
        ctx.fillStyle = player.color;
        for (let i = 0; i < player.snake.length; i++) {
            let cell = player.snake[i];
            // let image;
            // if (i == 0) {
            //     // tail
            //     image = SNAKE_HEAD_IMAGE;
            // } else if (i == player.snake.length - 1) {
            //     image = SNAKE_HEAD_IMAGE;
            //     // ctx.rotate(90);
            // }
            //     // image.style.transform = "rotate(180deg)";            } 
            // else {
            //     image = SNAKE_BODY_IMAGE;
            // }
            // ctx.drawImage(image, cell.x * size, cell.y * size, size, size)
            ctx.fillRect(cell.x * size, cell.y * size, size, size);
        }
    }
}

function onKeydown(e) {
    if (e.keyCode == 32 
        || e.keyCode == 37 
        || e.keyCode == 38 
        || e.keyCode == 39 
        || e.keyCode == 40) {
        socket.emit('keydown', e.keyCode);
    }
}

function startGame() {
    startScreen.style.display = "none";
    gameScreen.style.display = "block";

    canvas = document.getElementById("canvas");
    ctx = canvas.getContext('2d');

    canvas.width = canvas.height = 600;

    document.addEventListener('keydown', onKeydown);
}